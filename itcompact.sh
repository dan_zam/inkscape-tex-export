#!/bin/bash
# source itcompact.sh input.svg output.pdf
# TODO:
# - 

if [ "$#" -lt 1 ]; then
    echo "Illegal number of parameters 1"
    return
fi
if [ "$#" -gt 2 ]; then
    echo "Illegal number of parameters 2"
    return
fi

currentdir="$PWD"
echo "currentdir=$currentdir"

# workspace="$currentdir/itws"
workspace="$(mktemp -d)"
echo "workspace=$workspace"

scriptdir=$currentdir/$(dirname "$0")
echo "scriptdir=$scriptdir"

inputfile="$currentdir/$1"
echo "inputfile=$inputfile"
if [ ! -f "$inputfile" ]; then
    echo "File not found!"
    return
fi

outputname="$currentdir/itout.pdf"
if [ "$#" -eq 2 ]; then
    outputname="$2"
fi
echo "outputname=$outputname"

# pdftexname="$(mktemp)"
pdftexname="$workspace/ittmp.pdf"
# afourname="$(mktemp)"
afourname="ita4"
maintex="$scriptdir/main"

# mkdir $workspace
inkscape --without-gui --file=$inputfile --export-pdf=$pdftexname --export-latex
cd $workspace
# echo $PWD
pdflatex --jobname=$afourname $maintex
cd $currentdir
# echo $PWD
pdfcrop --margins '0' $workspace/$afourname $outputname
