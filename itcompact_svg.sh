#!/bin/bash
# source itcompact.sh input.svg output.pdf
# TODO:
# - 

if [ "$#" -lt 1 ]; then
    echo "Illegal number of parameters 1"
    return
fi
if [ "$#" -gt 2 ]; then
    echo "Illegal number of parameters 3"
    return
fi

source itcompact.sh $1 $2
pdf2svg $outputname "$outputname.svg"
