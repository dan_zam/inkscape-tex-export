#!/bin/bash
# source itcompact.sh input.svg output.pdf
# TODO:
# - 

if [ "$#" -lt 2 ]; then
    echo "Illegal number of parameters 1"
    return
fi
if [ "$#" -gt 3 ]; then
    echo "Illegal number of parameters 3"
    return
fi

source itcompact_svg.sh $2 $3
echo inkscape --file="$outputfile.svg" --export-png="$outputfile.png" --export-dpi=$1
